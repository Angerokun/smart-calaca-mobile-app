export interface Request {
    form_id: BigInteger;
    status: String;
    description: String;
    priority: String;
}
