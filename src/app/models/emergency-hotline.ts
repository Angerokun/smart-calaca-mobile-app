export interface EmergencyHotline {
    id: BigInteger;
    department_id: BigInteger;
    mobile_number: String;
    telephone: String,
    department_info: {}
}
