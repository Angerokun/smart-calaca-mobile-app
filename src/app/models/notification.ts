export interface Notification {
    id: String;
    type: String;
    notifiable_type: String;
    notifiable_id: BigInteger;
    data: {}
}
