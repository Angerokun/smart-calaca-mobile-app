export interface Form {
    id: BigInteger;
    deparment_id: BigInteger;
    name: String;
    description: String;
    department_info: {},
    requirements: {}
}
