export interface EmergencyRequest {
    id: BigInteger;
    user_id: BigInteger;
    department_id: BigInteger;
    area_incident: String;
    type_incident: String,
    remarks: String
}
