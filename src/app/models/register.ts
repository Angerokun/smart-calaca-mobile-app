export class Register {
    first_name: string;
    middle_name: string;
    last_name: string;
    birth_date: string;
    contact_no: string;
    address: string;
    name: string;
    email: string;
    password: string;
    password_confirmation: string;
}