import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Request } from 'src/app/models/request';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-track-record',
  templateUrl: './track-record.page.html',
  styleUrls: ['./track-record.page.scss'],
})
export class TrackRecordPage implements OnInit {
  @Input() reqId: BigInteger;
  request: Request;

  constructor(
    private modalController: ModalController,
    private requestService: RequestService
  ) { }

  ngOnInit() {
  }

  // Dismiss Login Modal
  dismissModal() {
    this.modalController.dismiss();
  }

  ionViewWillEnter() {
    this.requestService.showRequestedDocs(this.reqId).subscribe(
      request => {
        console.log(request);
        this.request = request;
      }
    );
  }

}
