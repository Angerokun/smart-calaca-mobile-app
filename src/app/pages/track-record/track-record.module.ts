import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TrackRecordPageRoutingModule } from './track-record-routing.module';

import { TrackRecordPage } from './track-record.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TrackRecordPageRoutingModule
  ],
  declarations: [TrackRecordPage]
})
export class TrackRecordPageModule {}
