import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Form } from 'src/app/models/form';
import { FormService } from 'src/app/services/form.service';
import { FormRequirementPage } from '../form-requirement/form-requirement.page';

@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss'],
})
export class FormPage implements OnInit {
  forms: Form;

  constructor(
    private formService: FormService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.formService.getForms().subscribe(
      form => {
        this.forms = form
        console.log(form);
      }
    );
  }

  async formShow(formId: BigInteger) {
    console.log('register');
    const fromShowModal = await this.modalController.create({
      component: FormRequirementPage,
      componentProps: { 
          formId: formId
      }
    });
    return await fromShowModal.present();
  }

}
