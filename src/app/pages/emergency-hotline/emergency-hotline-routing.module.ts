import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmergencyHotlinePage } from './emergency-hotline.page';

const routes: Routes = [
  {
    path: '',
    component: EmergencyHotlinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmergencyHotlinePageRoutingModule {}
