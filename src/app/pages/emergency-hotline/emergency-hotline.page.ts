import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { EmergencyHotline } from 'src/app/models/emergency-hotline';
import { EmergencyHotlineService } from 'src/app/services/emergency-hotline.service';
import { EmergencyReportPage } from '../emergency-report/emergency-report.page';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-emergency-hotline',
  templateUrl: './emergency-hotline.page.html',
  styleUrls: ['./emergency-hotline.page.scss'],
})
export class EmergencyHotlinePage implements OnInit {
  @Input() hotlineId: BigInteger;
  emergencyHotline: EmergencyHotline;

  constructor(
    private hotlineService: EmergencyHotlineService,
    private modalController: ModalController,
    private callNumber: CallNumber
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.hotlineService.showEmergencyHotline(this.hotlineId).subscribe(
      emergency_hotline => {
        console.log(emergency_hotline);
        this.emergencyHotline = emergency_hotline;
      }
    );
  }

  dismiss() {
    this.modalController.dismiss();
  }

  async emergencyReport(departmentId: BigInteger) {
    console.log('emergency Report');
    const reportModal = await this.modalController.create({
      component: EmergencyReportPage,
      componentProps: { 
        departmentId: departmentId
      }
    });
    return await reportModal.present();
  }

  callHotlineNumber(contactno: string) {
    this.callNumber.callNumber(contactno, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }


}
