import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmergencyHotlinePageRoutingModule } from './emergency-hotline-routing.module';

import { EmergencyHotlinePage } from './emergency-hotline.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmergencyHotlinePageRoutingModule
  ],
  declarations: [EmergencyHotlinePage]
})
export class EmergencyHotlinePageModule {}
