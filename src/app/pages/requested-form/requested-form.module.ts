import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequestedFormPageRoutingModule } from './requested-form-routing.module';

import { RequestedFormPage } from './requested-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RequestedFormPageRoutingModule
  ],
  declarations: [RequestedFormPage]
})
export class RequestedFormPageModule {}
