import { Component, OnInit } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { Request } from 'src/app/models/request';
import { RequestService } from 'src/app/services/request.service';
import { RequestPage } from '../request/request.page';

@Component({
  selector: 'app-requested-form',
  templateUrl: './requested-form.page.html',
  styleUrls: ['./requested-form.page.scss'],
})
export class RequestedFormPage implements OnInit {
  pet: string = "request";
  isAndroid: boolean = false;
  requests: Request;

  constructor(
    private platform: Platform,
    private requestService: RequestService,
    private modalController: ModalController
  ) 
  { 
    this.isAndroid = this.platform.is('android');
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.requestService.getRequestedDocs().subscribe(
      request => {
        console.log(request);
        this.requests = request;
      }
    );
  }

  async requestView(reqId: BigInteger) {
    console.log('request');
    const requestModal = await this.modalController.create({
      component: RequestPage,
      componentProps: { 
        reqId: reqId
      }
    });
    return await requestModal.present();
  }

}
