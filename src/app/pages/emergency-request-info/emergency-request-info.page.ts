import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { EmergencyRequest } from 'src/app/models/emergency-request';
import { EmergencyRequestService } from 'src/app/services/emergency-request.service';

@Component({
  selector: 'app-emergency-request-info',
  templateUrl: './emergency-request-info.page.html',
  styleUrls: ['./emergency-request-info.page.scss'],
})
export class EmergencyRequestInfoPage implements OnInit {
  @Input() requestId: BigInteger;
  emergencyRequest: EmergencyRequest;

  constructor(
    private modalController: ModalController,
    private requestService: EmergencyRequestService
  ) { }

  ngOnInit() { }

  dismiss() {
    this.modalController.dismiss();
  }

  ionViewWillEnter() {
    this.requestService.showEmergencyRequests(this.requestId).subscribe(
      emergency_request => {
        console.log(emergency_request);
        this.emergencyRequest = emergency_request;
      }
    );
  }

}
