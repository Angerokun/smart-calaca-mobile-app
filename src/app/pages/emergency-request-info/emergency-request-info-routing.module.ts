import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmergencyRequestInfoPage } from './emergency-request-info.page';

const routes: Routes = [
  {
    path: '',
    component: EmergencyRequestInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmergencyRequestInfoPageRoutingModule {}
