import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmergencyRequestInfoPageRoutingModule } from './emergency-request-info-routing.module';

import { EmergencyRequestInfoPage } from './emergency-request-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmergencyRequestInfoPageRoutingModule
  ],
  declarations: [EmergencyRequestInfoPage]
})
export class EmergencyRequestInfoPageModule {}
