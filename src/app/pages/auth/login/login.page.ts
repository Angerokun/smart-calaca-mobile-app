import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { RegisterPage } from '../register/register.page';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  form: FormGroup;
  
  constructor(
    private modalController: ModalController,
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private alertService: AlertService
  ) { }
  
  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    })
  }
  // Dismiss Login Modal
  dismissLogin() {
    this.modalController.dismiss();
  }
  // On Register button tap, dismiss login modal and open register modal
  async registerModal() {
    this.dismissLogin();
    const registerModal = await this.modalController.create({
      component: RegisterPage
    });
    return await registerModal.present();
  }
  
  async submitLogin() {
    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.authService.login(this.form.value).subscribe(
      data => {
        loading.dismiss();
        this.alertService.presentToast('Successfully Logged In');
      },
      error => {
        loading.dismiss();
        this.alertService.presentToast(error['error']['message']);
      },
      () => {
        loading.dismiss();
        this.dismissLogin();
        this.navCtrl.navigateRoot('/dashboard');
      }
    );

  }
}