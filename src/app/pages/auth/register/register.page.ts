import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { AuthService } from 'src/app/services/auth.service';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  form: FormGroup;

  constructor(private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private loadingCtrl: LoadingController
  ) { }
  ngOnInit() {
    this.form = new FormGroup({
      first_name: new FormControl(null, [Validators.required]),
      middle_name: new FormControl(null),
      last_name: new FormControl(null, [Validators.required]),
      birth_date: new FormControl(null, [Validators.required]),
      contact_no: new FormControl(null, [Validators.required]),
      address: new FormControl(null),
      name: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
      password_confirmation: new FormControl(null, [Validators.required])
    })
  }
  // Dismiss Register Modal
  dismissRegister() {
    this.modalController.dismiss();
  }
  // On Login button tap, dismiss Register modal and open login Modal
  async loginModal() {
    this.dismissRegister();
    const loginModal = await this.modalController.create({
      component: LoginPage,
    });
    return await loginModal.present();
  }
  async register() {
    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.authService.register(this.form.value).subscribe(
      data => {
        this.authService.login(this.form.value).subscribe(
          data => {
            loading.dismiss();
            this.alertService.presentToast('Successfully Created!');
          },
          error => {
            loading.dismiss();
            this.alertService.presentToast(error['error']['message']);
          },
          () => {
            loading.dismiss();
            this.dismissRegister();
            this.navCtrl.navigateRoot('/dashboard');
          }
        );
        loading.dismiss();
        this.alertService.presentToast(data['message']);
      },
      error => {
        loading.dismiss();
        this.alertService.presentToast(JSON.stringify(error['error']['errors']));
      },
      () => {
        
      }
    );
  }
}