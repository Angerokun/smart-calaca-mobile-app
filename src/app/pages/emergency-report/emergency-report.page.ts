import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert.service';
import { EmergencyRequestService } from 'src/app/services/emergency-request.service';
import { EmergencyRequestInfoPage } from '../emergency-request-info/emergency-request-info.page';

@Component({
  selector: 'app-emergency-report',
  templateUrl: './emergency-report.page.html',
  styleUrls: ['./emergency-report.page.scss'],
})
export class EmergencyReportPage implements OnInit {
  @Input() departmentId: BigInteger;
  form: FormGroup;

  constructor(
    private modalController: ModalController,
    private emergencyRequest: EmergencyRequestService,
    private loadingCtrl: LoadingController,
    private alertService: AlertService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      area_incident: new FormControl(null, [Validators.required]),
      remarks: new FormControl(null, [Validators.required]),
      type_incident: new FormControl(null, [Validators.required]),
      department_id: new FormControl(null, [Validators.required])
    })

    this.form.get('department_id').setValue(this.departmentId);
  }

  dismissReport() {
    this.modalController.dismiss();
  }

  async postEmergencyReport(){
    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();

    this.emergencyRequest.emergencyRequest(this.form.value).subscribe(
      data => {
        loading.dismiss();
        this.alertService.presentToast('Emergency Report Successfully Sent!');
        this.dismissReport();
        this.navCtrl.navigateRoot('/emergency-request');
      },
      error => {
        loading.dismiss();
        this.alertService.presentToast(JSON.stringify(error['error']['errors']));
      },
      () => {
        
      }
    );
  }
}
