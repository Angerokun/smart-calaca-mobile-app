import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmergencyReportPageRoutingModule } from './emergency-report-routing.module';

import { EmergencyReportPage } from './emergency-report.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EmergencyReportPageRoutingModule
  ],
  declarations: [EmergencyReportPage]
})
export class EmergencyReportPageModule {}
