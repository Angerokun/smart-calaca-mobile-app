import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmergencyReportPage } from './emergency-report.page';

const routes: Routes = [
  {
    path: '',
    component: EmergencyReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmergencyReportPageRoutingModule {}
