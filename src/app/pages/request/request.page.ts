import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileChooser} from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AuthService } from 'src/app/services/auth.service';
import { RequestService } from 'src/app/services/request.service';
import { Request } from 'src/app/models/request';
import { AlertService } from 'src/app/services/alert.service';
import { TrackRecordPage } from '../track-record/track-record.page';
import { EnvService } from 'src/app/services/env.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.page.html',
  styleUrls: ['./request.page.scss'],
})
export class RequestPage implements OnInit {
  @Input() reqId: BigInteger;
  today: any;
  uploadText: any;
  downloadText: any;
  fileTransfer: FileTransferObject;
  request: Request;
  docDl: any;

  constructor(
    private modalController: ModalController,
    private transfer: FileTransfer,
    private file: File,
    private filePath: FilePath,
    private fileChooser: FileChooser,
    private authService: AuthService,
    private requestService: RequestService,
    private loadingCtrl: LoadingController,
    private alertService: AlertService,
    private env: EnvService
  ) {
    this.uploadText = 'Upload';
    this.downloadText = '';
    this.docDl = `${this.env.API_URL}/download-docs-activities/`;
   }

  ngOnInit() {
    this.today = Date.now();
  }

  ionViewWillEnter() {
    this.requestService.showRequestedDocs(this.reqId).subscribe(
      request => {
        console.log(request);
        this.request = request;
      }
    );
  }

  // Dismiss Login Modal
  dismissReq() {
    this.modalController.dismiss();
  }

  async uploadFile() {
    const loading = await this.loadingCtrl.create({message: 'Loading....'});
     loading.present();
     this.fileChooser.open().then((uri)=> {
      this.filePath.resolveNativePath(uri).then(
      (nativepath)=>{
        this.fileTransfer = this.transfer.create();
        let options: FileUploadOptions={
          fileKey: 'file',
          fileName: nativepath.substr(nativepath.lastIndexOf('/') + 1),
          chunkedMode: false,
          mimeType: "multipart/form-data", // add mimeType
          //params : {'bid':businessId,"imgurl":theFile,"content":content},
          headers: {
            'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
          }
        }
        this.uploadText = "Uploading...";
        
        this.fileTransfer.upload(
          nativepath,
          encodeURI('http://132.148.19.222/smart-calaca-ws/public/api/request-docs/' + this.authService.token["user"]["id"] + '/documents/' + this.reqId),
          options
        ).then((data) => {
          loading.dismiss();
          this.alertService.presentToast("File Successfully Uploaded!");
          this.uploadText = "Upload";
        }, (err) => {
          this.uploadText = "Upload";
          loading.dismiss();
          alert("There's something while uploading, upload reach limit 2MB or internet connection problem.");
        })
      }, (err) => {
        loading.dismiss();
        alert("error2: " +JSON.stringify(err));
      })
    },(err)=>{
      loading.dismiss();
      alert("error3: " +JSON.stringify(err));
    })
  }

  abortUpload() {
    this.fileTransfer.abort();
    alert("upload cancel.");
  }

  async trackingRecord(reqId: BigInteger) {
    console.log('track-record');
    const trackingModal = await this.modalController.create({
      component: TrackRecordPage,
      componentProps: { 
        reqId: reqId
      }
    });
    return await trackingModal.present();
  }
}
