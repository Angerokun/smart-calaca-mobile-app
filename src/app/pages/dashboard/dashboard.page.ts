import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { FormService } from 'src/app/services/form.service';
import { Form } from 'src/app/models/form';
import { FormRequirementPage } from '../form-requirement/form-requirement.page';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  user: User;
  forms: Form;

  constructor(
    private menu: MenuController,
    private authService: AuthService,
    private formService: FormService,
    private modalController: ModalController,
    private navCtrl: NavController
  ) { 
    this.menu.enable(true);
  }
  ngOnInit() {
    
  }
  ionViewWillEnter() {
    this.authService.user().subscribe(
      user => {
        console.log(user);
        this.user = user;
        this.getForms();
      }
    );
  }

  getForms() {
    this.formService.getForms().subscribe(
      form => {
        this.forms = form
        console.log(form);
      }
    );
  }

  async formShow(formId: BigInteger) {
    console.log('register');
    const fromShowModal = await this.modalController.create({
      component: FormRequirementPage,
      componentProps: { 
          formId: formId
      }
    });
    return await fromShowModal.present();
  }

  requestForm()
  {
    this.navCtrl.navigateRoot(['/form'])
  }

  emergencyRequest()
  {
    this.navCtrl.navigateRoot(['/emergency-request'])
  }
}