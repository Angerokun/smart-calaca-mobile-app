import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Notification } from 'src/app/models/notification';
import { NotificationService } from 'src/app/services/notification.service';
import { RequestPage } from '../request/request.page';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {
  notifications: Notification;

  constructor(
    private notificationService: NotificationService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.notificationService.getNotifications().subscribe(
      notification => {
        console.log(notification);
        this.notifications = notification;
      }
    );
  }

  async requestView(reqId: BigInteger) {
    console.log('request');
    const requestModal = await this.modalController.create({
      component: RequestPage,
      componentProps: { 
        reqId: reqId
      }
    });
    return await requestModal.present();
  }

}
