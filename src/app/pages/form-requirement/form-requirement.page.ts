import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { Form } from 'src/app/models/form';
import { FormService } from 'src/app/services/form.service';
import { EnvService } from 'src/app/services/env.service';
import { AuthService } from 'src/app/services/auth.service';
import { RequestPage } from '../request/request.page';
import { RequestService } from 'src/app/services/request.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-form-requirement',
  templateUrl: './form-requirement.page.html',
  styleUrls: ['./form-requirement.page.scss'],
})
export class FormRequirementPage implements OnInit {

  @Input() formId: BigInteger;
  formRequirements: Form;
  form: FormGroup;

  constructor(
    private formService: FormService,
    private modalController: ModalController,
    private requestService: RequestService,
    private loadingCtrl: LoadingController,
    private alertService: AlertService,
    private navCtrl: NavController
  ) {
    this.form = new FormGroup({
      form_id: new FormControl(null, [Validators.required])
    })
    
   }

  ngOnInit() {
    console.log(this.formId);
    this.form.patchValue({form_id: this.formId});
  }
  // Dismiss Login Modal
  dismissRequirements() {
    this.modalController.dismiss();
  }

  ionViewWillEnter() {
    this.formService.getFormRequirements(this.formId).subscribe(
      form => {
        console.log(form);
        this.formRequirements = form;
      }
    );
  }

  async request() {
    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.requestService.requestDocs(this.form.value).subscribe(
      data => {
        console.log(data);
        loading.dismiss();
        this.alertService.presentToast('Successfully Request!');
        this.dismissRequirements();
        this.navCtrl.navigateRoot('/requested-form');
      },
      error => {
        loading.dismiss();
        this.alertService.presentToast(error['error']['message']);
      })
    // console.log('request');
    // const fromShowModal = await this.modalController.create({
    //   component: RequestPage,
    //   componentProps: { 
    //       formId: this.formId
    //   }
    // });
    // return await fromShowModal.present();
  }
}
