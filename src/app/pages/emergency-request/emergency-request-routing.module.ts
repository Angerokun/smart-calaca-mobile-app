import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmergencyRequestPage } from './emergency-request.page';

const routes: Routes = [
  {
    path: '',
    component: EmergencyRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmergencyRequestPageRoutingModule {}
