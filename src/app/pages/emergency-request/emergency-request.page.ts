import { Component, OnInit } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { EmergencyHotline } from 'src/app/models/emergency-hotline';
import { EmergencyRequest } from 'src/app/models/emergency-request';
import { EmergencyHotlineService } from 'src/app/services/emergency-hotline.service';
import { EmergencyRequestService } from 'src/app/services/emergency-request.service';
import { EmergencyHotlinePage } from '../emergency-hotline/emergency-hotline.page';
import { EmergencyRequestInfoPage } from '../emergency-request-info/emergency-request-info.page';

@Component({
  selector: 'app-emergency-request',
  templateUrl: './emergency-request.page.html',
  styleUrls: ['./emergency-request.page.scss'],
})
export class EmergencyRequestPage implements OnInit {
  start: string = "emergency";
  isAndroid: boolean = false;
  emergencyRequest: EmergencyRequest;
  emergencyHotline: EmergencyHotline;

  constructor(
    private platform: Platform,
    private emergencyService: EmergencyRequestService,
    private hotlineService: EmergencyHotlineService,
    private modalController: ModalController
  ) {
    this.isAndroid = this.platform.is('android');
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.emergencyService.getEmergencyRequests().subscribe(
      emergency_request => {
        console.log(emergency_request);
        this.emergencyRequest = emergency_request;
      }
    );

    this.hotlineService.getEmergencyHotline().subscribe(
      emergency_hotline => {
        console.log(emergency_hotline);
        this.emergencyHotline = emergency_hotline;
      }
    );
  }

  async hotlineView(hotlineId: BigInteger) {
    console.log('request');
    const requestModal = await this.modalController.create({
      component: EmergencyHotlinePage,
      componentProps: { 
        hotlineId: hotlineId
      }
    });
    return await requestModal.present();
  }

  async requestView(requestId: BigInteger) {
    console.log('request');
    const requestEmergencyModal = await this.modalController.create({
      component: EmergencyRequestInfoPage,
      componentProps: { 
        requestId: requestId
      }
    });
    return await requestEmergencyModal.present();
  }

}
