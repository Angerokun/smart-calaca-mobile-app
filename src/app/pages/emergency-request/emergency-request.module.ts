import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmergencyRequestPageRoutingModule } from './emergency-request-routing.module';

import { EmergencyRequestPage } from './emergency-request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmergencyRequestPageRoutingModule
  ],
  declarations: [EmergencyRequestPage]
})
export class EmergencyRequestPageModule {}
