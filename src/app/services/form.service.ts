import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Form } from '../models/form';
import { AuthService } from './auth.service';
import { EnvService } from './env.service';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private authService: AuthService
  ) { }


  getForms(){
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.get<Form>(`${this.env.API_URL}/forms`, { headers: headers })
    .pipe(
      tap(forms => {
        return forms;
      })
    )
  }

  getFormRequirements(formId: BigInteger) {
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.get<Form>(`${this.env.API_URL}/forms/`+ formId, { headers: headers })
    .pipe(
      tap(form => {
        return form;
      })
    )
  }
  
}
