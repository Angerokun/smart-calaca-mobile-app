import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { EmergencyRequest } from '../models/emergency-request';
import { AuthService } from './auth.service';
import { EnvService } from './env.service';

@Injectable({
  providedIn: 'root'
})
export class EmergencyRequestService {

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private authService: AuthService
  ) { }

  emergencyRequest(emergencyRequest: EmergencyRequest): Observable<EmergencyRequest> {
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.post<EmergencyRequest>(
        `${this.env.API_URL}/emergency-requests/` + this.authService.token["user"]["id"], emergencyRequest,
        { headers: headers }
    ).pipe(
      tap(request => {
        return request;
      })
    )
  }

  getEmergencyRequests() {
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.get<EmergencyRequest>(
      `${this.env.API_URL}/emergency-requests/`+ this.authService.token["user"]["id"],
      { headers: headers }
    ).pipe(
      tap(request => {
        return request;
      })
    )
  }

  showEmergencyRequests(requestId: BigInteger) {
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.get<EmergencyRequest>(
      `${this.env.API_URL}/emergency-requests/`+ this.authService.token["user"]["id"] +  `/requests/` + requestId,
      { headers: headers }
    ).pipe(
      tap(request => {
        return request;
      })
    )
  }
}
