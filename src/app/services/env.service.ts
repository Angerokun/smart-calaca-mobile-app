import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {
  //API_URL = 'http://localhost/smart-calaca-ws/public/api';

  API_URL = 'http://132.148.19.222/smart-calaca-ws/public/api';

  constructor() { }
}
