import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; 
import { tap } from 'rxjs/operators';
import { Request } from '../models/request';
import { AuthService } from './auth.service';
import { EnvService } from './env.service';
import { DocumentRequest } from '../models/document';

@Injectable({
  providedIn: 'root'
})
export class RequestService {


  constructor(
    private http: HttpClient,
    private env: EnvService,
    private authService: AuthService
  ) { }

  requestDocs(request: Request): Observable<Request> {
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.post<Request>(`${this.env.API_URL}/request-docs/` + this.authService.token["user"]["id"], request, { headers: headers })
    .pipe(
      tap(request => {
        return request;
      })
    )
  }


  getRequestedDocs() {
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.get<Request>(`${this.env.API_URL}/request-docs/`+ this.authService.token["user"]["id"], { headers: headers })
    .pipe(
      tap(request => {
        return request;
      })
    )
  }

  showRequestedDocs(reqId: BigInteger) {
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.get<Request>(
      `${this.env.API_URL}/request-docs/`+ this.authService.token["user"]["id"] + `/requested/` + reqId,
      { headers: headers }
    )
    .pipe(
      tap(request => {
        return request;
      })
    )
  }

  uploadDocument(options, requestId) {
    // const headers = new HttpHeaders({
    //   'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    // });
    return this.http.post(`${this.env.API_URL}/request-docs/` + this.authService.token["user"]["id"] + `/documents/` + requestId, options)
    .pipe(
      tap(request => {
        return request;
      })
    )
  }
}
