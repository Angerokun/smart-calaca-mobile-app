import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Notification } from '../models/notification';
import { AuthService } from './auth.service';
import { EnvService } from './env.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private authService: AuthService
  ) { }

  getNotifications() {
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.get<Notification>(`${this.env.API_URL}/notifications`, { headers: headers })
    .pipe(
      tap(notification => {
        return notification;
      })
    )
  }
}
