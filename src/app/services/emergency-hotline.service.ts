import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { EmergencyHotline } from '../models/emergency-hotline';
import { AuthService } from './auth.service';
import { EnvService } from './env.service';

@Injectable({
  providedIn: 'root'
})
export class EmergencyHotlineService {

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private authService: AuthService
  ) { }

  getEmergencyHotline() {
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.get<EmergencyHotline>(
      `${this.env.API_URL}/hotlines`,
      { headers: headers }
    ).pipe(
      tap(hotline => {
        return hotline;
      })
    )
  }

  showEmergencyHotline(hotline: BigInteger) {
    const headers = new HttpHeaders({
      'Authorization': this.authService.token["token_type"]+" "+this.authService.token["token"]
    });
    return this.http.get<EmergencyHotline>(
      `${this.env.API_URL}/hotlines/` + hotline,
      { headers: headers }
    ).pipe(
      tap(hotline => {
        return hotline;
      })
    )
  }
}
