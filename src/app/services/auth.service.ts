import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';  
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvService } from './env.service';
import { User } from '../models/user';
import { Login } from '../models/login';
import { Register } from '../models/register';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token:any;
  private userData = new Subject<any>();
  
  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvService,
  ) { }

  /**
   * 
   * @param login
   * @returns 
   */
  login(login: Login): Observable<Login> {
    return this.http.post<Login>(`${this.env.API_URL}/auth/login`, login).pipe(
      tap(token => {
        console.log(token);
        this.storage.setItem('token', token)
        .then(
          () => {
            console.log('Token Stored');
          },
          error => console.error('Error storing item', error)
        );
        this.token = token;
        this.isLoggedIn = true;
        return token;
      }),
    );
  }

  getObservable(): Subject<any> {
      return this.userData;
  }
  
  /**
   * 
   * @param register 
   * @returns 
   */
  register(register: Register): Observable<Register> {
    return this.http.post<Register>(`${this.env.API_URL}/auth/register`, register);
  }
  
  /**
   * 
   * @returns 
   */
  logout() {
    const headers = new HttpHeaders({
      'Authorization': this.token["token_type"]+" "+this.token["token"]
    });
    return this.http.get(`${this.env.API_URL}/auth/logout`, { headers: headers })
    .pipe(
      tap(data => {
        this.storage.remove("token");
        this.isLoggedIn = false;
        delete this.token;
        return data;
      })
    )
  }

  /**
   * 
   * @returns 
   */
  user(): Observable<User> {
    const headers = new HttpHeaders({
      'Authorization': this.token["token_type"]+" "+this.token["token"]
    });
    return this.http.get<User>(`${this.env.API_URL}/auth/user-profile`, { headers: headers })
    .pipe(
      tap(user => {
        this.userData.next(user);
        return user;
      })
    )
  }

  /**
   * 
   * @returns 
   */
  getToken() {
    return this.storage.getItem('token').then(
      data => {
        this.token = data;
        if(this.token != null) {
          this.isLoggedIn=true;
        } else {
          this.isLoggedIn=false;
        }
      },
      error => {
        this.token = null;
        this.isLoggedIn=false;
      }
    );
  }
}
