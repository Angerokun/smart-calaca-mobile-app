import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { LandingGuard } from './guard/landing.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'landing',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'landing',
    loadChildren: () => import('./pages/landing/landing.module').then( m => m.LandingPageModule),
    canActivate: [LandingGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/auth/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'form-requirement',
    loadChildren: () => import('./pages/form-requirement/form-requirement.module').then( m => m.FormRequirementPageModule)
  },
  {
    path: 'requested-form',
    loadChildren: () => import('./pages/requested-form/requested-form.module').then( m => m.RequestedFormPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./pages/notification/notification.module').then( m => m.NotificationPageModule)
  },
  {
    path: 'request',
    loadChildren: () => import('./pages/request/request.module').then( m => m.RequestPageModule)
  },
  {
    path: 'track-record',
    loadChildren: () => import('./pages/track-record/track-record.module').then( m => m.TrackRecordPageModule)
  },
  {
    path: 'emergency-request',
    loadChildren: () => import('./pages/emergency-request/emergency-request.module').then( m => m.EmergencyRequestPageModule)
  },
  {
    path: 'emergency-hotline',
    loadChildren: () => import('./pages/emergency-hotline/emergency-hotline.module').then( m => m.EmergencyHotlinePageModule)
  },
  {
    path: 'emergency-report',
    loadChildren: () => import('./pages/emergency-report/emergency-report.module').then( m => m.EmergencyReportPageModule)
  },
  {
    path: 'emergency-request-info',
    loadChildren: () => import('./pages/emergency-request-info/emergency-request-info.module').then( m => m.EmergencyRequestInfoPageModule)
  },
  {
    path: 'form',
    loadChildren: () => import('./pages/form/form.module').then( m => m.FormPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
